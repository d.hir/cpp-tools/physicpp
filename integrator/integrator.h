////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////
////  INTEGRATOR.H
////  ============
////
////  HEADER FILE FOR NUMERICAL INTEGRATION
////  USAGE FOR INTEGRATING VECTOR V OF TYPE C WITH SPACING H:
////
////  Integrator<C> i (IntegrationTechnique t);
////  C result = i.integrate(V, H);
////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifndef INTEGRATOR_H
#define INTEGRATOR_H

// FOR USING ITERATORS TO ACCESS DATA STRUCTURES (BETTER COMPATABILITY!)
#include <iterator>
// FOR PRINTING TO STDOUT (ERROR-HANDLING AND DEBUGGING)
#include <iostream>
// FOR INTERNAL DATA HANDLING / STANDARD CONTAINER RETURN TYPE
#include <vector>

// FOR GAUSS INTEGRATION TECHNIQUE
#include "specialpolynomial.h"
// FOR INTEGRAL RANGE TRANSFORMATION
#include "transformer.h"
// FOR STANDARD COMPLEX TEMPLATE ARGUMENT
#include <complex>

// ENUMERATION LIST OF ALL POSSIBLE VALUES OF INTEGRATION TECHNIQUES
enum class IntegrationTechnique {
  trapez=0, Trapez=0, trapezoid=0, trapezoidal=0,
  simpson13=1, simpson_1_3=1, Simpson13=1, Simpson_1_3=1, simpson1_3=1, 
    Simpson1_3=1, simpson_13=1, Simpson_13=1,
  gauss=2, Gauss=2,
  rawgauss=3, RawGauss=3, raw_gauss=3, baregauss=3, BareGauss=3, bare_gauss=3
};

// FORWARD DECLARATION OF DIFFERENT INTEGRATION METHODS
class IntMethod;
class IntMethodTrapez;
class IntMethodSimpson13;
class IntMethodGauss;
class IntMethodRawGauss;

////////////////////////////////////////////////////////////////////////////////
// 
// RANGE STRUCT
// ============
//
// STRUCT USED TO COMBINE UPPER AND LOWER BOUND TO A SINGE OBJECT
//
////////////////////////////////////////////////////////////////////////////////
template<typename C>
struct Range {
  // LOWER BOUND
  C from;
  // UPPER BOUND
  C to;
  // BOOLEAN TO KEEP TRACK IF RANGE IS VALID AND USABLE
  bool is_valid;
  
  //////////////////////////////////////////////////////////////////////////////
  // RANGE::RANGE
  // ------------
  // PLACEHOLDER CONSTRUCTOR FOR CREATING A RANGE. OBJECTS CREATED THIS WAY 
  // MUST NOT BE USED WITHOUT EXPLICITLY SETTING 'to' AND 'from' MEMBERS.
  //
  // IN:
  //   -
  // OUT:
  //   (RANGE) WHICH IS DECLARED INVALID
  //////////////////////////////////////////////////////////////////////////////
  Range() : is_valid(false) {};
  
  //////////////////////////////////////////////////////////////////////////////
  // RANGE::RANGE
  // ------------
  // STANDARD CONSTRUCTOR FOR RANGE STRUCT
  //
  // IN:
  //   from_ (C) ... LOWER BOUND OF RANGE
  //   to_   (C) ... UPPER BOUND OF RANGE
  // OUT:
  //   (RANGE)
  //////////////////////////////////////////////////////////////////////////////
  Range(C from_, C to_) : from(from_), to(to_), is_valid(true) {};
};

////////////////////////////////////////////////////////////////////////////////
// 
// INTVISITOR CLASS
// ================
//
// VISITOR SYSTEM ABSTRACT CLASS TO BE ABLE TO USE TEMPLATES AND POLYMORPHISM AT
// THE SAME TIME
// THIS IS NOT (!) A TEMPLATE CLASS!
//
////////////////////////////////////////////////////////////////////////////////
class IntVisitor {
  public:
    // PURELY VIRTUAL METHODS WHERE THE ACTUAL INTEGRATION WILL HAPPEN
    virtual void visit(const IntMethodTrapez&        int_method) = 0;
    virtual void visit(const IntMethodSimpson13&     int_method) = 0;
    virtual void visit(const IntMethodGauss&         int_method) = 0;
    virtual void visit(const IntMethodRawGauss&      int_method) = 0;
};

////////////////////////////////////////////////////////////////////////////////
// 
// INTSUBVISITOR CLASS : INTVISITOR
// ================================
//
// VISITOR SYSTEM SUB CLASS TO BE ABLE TO USE TEMPLATES AND POLYMORPHISM AT
// THE SAME TIME
// THIS IS A TEMPLATE CLASS DERIVED FROM THE NON-TEMPLATE INTVISITOR CLASS
//
////////////////////////////////////////////////////////////////////////////////
template<typename O, typename C, typename OV, typename CV>
class IntSubVisitor : public IntVisitor {
  private:
    // CONTAINER OF x VALUES
    OV x;
    // CONTAINER OF y VALUES
    CV y;
    // WIDTH OF A SINGLE INTERVAL USED E.G. FOR TRAPEZOIDAL METHOD
    O h;
    // POLYNOMIAL TO USE FOR GAUSS INTEGRATION TECHNIQUE
    SpecialPolynomial<O>* polynomial_ptr;
    // MEMBER WHERE THE RESULT WILL BE STORED (AND CAN THEN BE RETRIEVED)
    C result;
    // WHAT PARAMETERS ARE GIVEN TO THE INTEGRATION
    // xy ... (x, y) DOUBLETS OF POINTS
    // yh ... y VECTOR WITH SPACING h
    enum class CallMode {xy, yh} mode;
    // RANGE OF INTEGRATION
    Range<O> range;
    // WEIGHTS FOR GAUSS INTEGRATION
    std::vector<O> weights;
    // JACOBIAN DETERMINANTS FOR VARIABLE TRANSFORMATIONS
    std::vector<O> jacobians;
    // KEEP TRACK WHETHER ALL PARAMETERS ARE SET
    bool is_valid;
    
  public:
    ////////////////////////////////////////////////////////////////////////////
    // INTSUBVISITOR::INTSUBVISITOR
    // ----------------------------
    // CONSTRUCTOR FOR CREATING A TEMPORARY, INVALID OBJECT
    //
    // IN:
    //   -
    // OUT:
    //   (INTSUBVISITOR) INVALID OBJECT!
    ////////////////////////////////////////////////////////////////////////////
    IntSubVisitor() : is_valid(false) {};
    
    ////////////////////////////////////////////////////////////////////////////
    // INTSUBVISITOR::INTSUBVISITOR
    // ----------------------------
    // NORMAL CONSTRUCTOR
    //
    // IN:
    //   p_ptr_ (SPECIALPOLYNOMIAL<C>*) ... POLYNOMIAL FOR GAUSS TECHNIQUE
    //   range_ (RANGE) ... RANGE OF INTEGRATION
    // OUT:
    //   (INTSUBVISITOR)
    ////////////////////////////////////////////////////////////////////////////
    IntSubVisitor(SpecialPolynomial<O>* p_ptr_, Range<O> range_) : 
        polynomial_ptr(p_ptr_), range(range_), is_valid(false) {};
    
    ////////////////////////////////////////////////////////////////////////////
    // INTSUBVISITOR::SETDATA
    // ----------------------
    // SET INTEGRATION VALUS FOR INTEGRATION IN xy MODE
    //
    // IN:
    //   X_     (CV&)    ... CONTAINER OF x VALUES
    //   y_     (CV&)    ... CONTAINER OF y VALUES
    // OUT:
    //   -
    ////////////////////////////////////////////////////////////////////////////
    void setData(const OV& x_, const CV& y_) {
      this->x = x_;
      this->y = y_;
      this->is_valid = true;
      this->mode = CallMode::xy;
    };
    
    ////////////////////////////////////////////////////////////////////////////
    // INTSUBVISITOR::SETDATA
    // ----------------------
    // SET INTEGRATION VALUS FOR INTEGRATION IN yh MODE
    //
    // IN:
    //   y_     (CV&)    ... CONTAINER OF y VALUES
    //   X_     (C)     ... SPACING OF x VALUES
    // OUT:
    //   -
    ////////////////////////////////////////////////////////////////////////////
    void setData(const CV& y_, O h_)  {
      this->y = y_;
      this->h = h_;
      this->is_valid = true;
      this->mode = CallMode::yh;
    };
    
    ////////////////////////////////////////////////////////////////////////////
    // INTSUBVISITOR::VISIT
    // --------------------
    // VISITOR CALL FUNCTION FOR TRAPEZOIDAL METHOD
    //
    // IN:
    //   int_method (INTMETHODTRAPEZ&) ... OBJECT FOR DISTINCTION BETWEEN
    //                                     INTEGRATION METHODS
    // OUT:
    //   -
    ////////////////////////////////////////////////////////////////////////////
    virtual void visit(const IntMethodTrapez& int_method) {
      // ACT DIFFERENTLY DEPENDING ON INPUT ARGUMENTS TO INTEGRATION
      switch (this->mode) {
        case CallMode::yh:
          // SET RESULT TO 0 FOR INITIALIZATION
          result = static_cast<C>(0);
          // SUM UP ALL y VALUES
          for (auto& y_elem : y)
            result = result + y_elem;
          // SUBTRACT 0.5 TIMES FIRST AND LAST ENTRY
          result = result - static_cast<C>(0.5) * *y.begin();
          result = result - static_cast<C>(0.5) * *(y.end() - 1);
          // MULTIPLY BY INTERVAL WIDTH h
          result = result * h;
          break;
        case CallMode::xy:
          std::cout << "xy mode not yet implemented" << std::endl;
          result = C(0);
          break;
        default:
          std::cout << "Invalid CallMode" << std::endl;
          result = C(0);
          break;
      }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // INTSUBVISITOR::VISIT
    // --------------------
    // VISITOR CALL FUNCTION FOR SIMPSON 1/3 METHOD
    //
    // IN:
    //   int_method (INTMETHODSIMPSON13&) ... OBJECT FOR DISTINCTION BETWEEN
    //                                        INTEGRATION METHODS
    // OUT:
    //   -
    ////////////////////////////////////////////////////////////////////////////
    virtual void visit(const IntMethodSimpson13& int_method) {
      // ACT DIFFERENTLY DEPENDING ON INPUT ARGUMENTS TO INTEGRATION
      switch (this->mode) {
        case CallMode::yh:
          // INITIALIZE RESULT TO THE FIRST ENTRY IN y
          result = *y.begin();
          // ADD 4 TIMES EVERY SECOND ENTRY TO TOTAL SUM
          for (auto it = ++y.begin(); it < (y.end() - 1); it += 2) {
            result = result + C(4) * *it;
          }
          // ADD 2 TIMES EACH OTHER ENTRY
          for (auto it = y.begin() + 2; it < (y.end() - 1); it += 2) {
            result = result + C(2) * *it;
          }
          // ADD LAST TERM ONCE
          result = result + *(--y.end());
          // MULTIPLY RESULT BY h/3
          result = result * h / C(3);
          break;
        case CallMode::xy:
          std::cout << "xy mode not yet implemented" << std::endl;
          result = C(0);
          break;
        default:
          std::cout << "Invalid CallMode" << std::endl;
          result = C(0);
          break;
      }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // INTSUBVISITOR::VISIT
    // --------------------
    // VISITOR CALL FUNCTION FOR FULL GAUSS QUADRATURE IN [a, b]
    //
    // IN:
    //   int_method (INTMETHODGAUSS&) ... OBJECT FOR DISTINCTION BETWEEN
    //                                    INTEGRATION METHODS
    // OUT:
    //   -
    ////////////////////////////////////////////////////////////////////////////
    virtual void visit(const IntMethodGauss& int_method) {
      // ACT DIFFERENTLY DEPENDING ON INPUT ARGUMENT TO INTEGRATION
      switch (this->mode) {
        case CallMode::yh:
          std::cout << "yh mode not yet implemented" << std::endl;
          result = C(0);
          break;
        case CallMode::xy: {
          // INITIALIZE RESULT TO 0 
          result = C(0);
          // ONLY CALCULATE WEIGHTS AND JACOBIANS IF NOT ALREADY DONE
          if (weights.size() != x.size()) {
            // NEED INVERSELY TRANSFORMED x VALUES FOR THE WEIGHTS
            LogarithmicTransformer<O> tr(range.from, range.to);
            // GET x IN RANGE [-1, 1]
            std::vector<O> x_itr = tr.inverseTransform(x);
            // CALCULATE WEIGHTS WITH x VALUES AND ORDER x.size()
            weights = polynomial_ptr->weights(x_itr, x.size());
            // GET JACOBIAN DETERMINANTS OF TRANSFORMATION
            jacobians = tr.getJacobian(x, true);
          }
          // ADD UP ALL WEIGHTS, JACOBIANS AND yi
          for (int j = 0; j < (int) x.size(); j++)
            result = result + jacobians[j] * y[j] * weights[j];
          break;
        }
        default:
          std::cout << "Invalid CallMode" << std::endl;
          result = C(0);
          break;
      }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // INTSUBVISITOR::VISIT
    // --------------------
    // VISITOR CALL FUNCTION FOR RAW GAUSS QUADRATURE IN [-1, 1]
    //
    // IN:
    //   int_method (INTMETHODRAWGAUSS&) ... OBJECT FOR DISTINCTION BETWEEN
    //                                       INTEGRATION METHODS
    // OUT:
    //   -
    ////////////////////////////////////////////////////////////////////////////
    virtual void visit(const IntMethodRawGauss& int_method) {
      // ACT DIFFERENTLY DEPENDING ON INPUT ARGUMENT TO INTEGRATION
      switch (this->mode) {
        case CallMode::yh:
          std::cout << "yh mode not yet implemented" << std::endl;
          result = C(0);
          break;
        case CallMode::xy: {
          // INITIALIZE RESULT TO 0 
          result = C(0);
          // ONLY CALCULATE WEIGHTS IF NOT ALREADY DONE
          if (weights.size() != x.size())
            weights = polynomial_ptr->weights(x, x.size());
          // ADD UP ALL WEIGHTS AND yi
          for (int j = 0; j < (int) (x.end() - x.begin()); j++)
            result = result + y[j] * weights[j];
          break;
        }
        default:
          std::cout << "Invalid CallMode" << std::endl;
          result = C(0);
          break;
      }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // INTSUBVISITOR::GETRESULT
    // ------------------------
    // GETTER METHOD FOR GETTING THE RESULT OF THE INTEGRATION
    // RESULT WILL BE 0 IF INTEGRATION FAILED
    // 
    // IN:
    //   -
    // OUT:
    //   (C) RESULT OF THE INTEGRATION
    ////////////////////////////////////////////////////////////////////////////
    C getResult() const {
      return this->result;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // INTSUBVISITOR::ISVALID
    // ----------------------
    // CHECKS WHETHER THIS INTSUBVISITOR OBJECT HAS ALL NECESSARY PARAMETERS SET
    //
    // IN:
    //   -
    // OUT:
    //   (BOOL) 'true' IF VALID, 'false' OTHERWISE
    ////////////////////////////////////////////////////////////////////////////
    bool isValid() {
      return this->is_valid;
    }
};

////////////////////////////////////////////////////////////////////////////////
// 
// INTMETHOD CLASS
// ===============
//
// SUPER CLASS USED FOR DIFFERENTIATING BETWEEN VARIOUS MODES OF INTEGRATION 
//
////////////////////////////////////////////////////////////////////////////////
class IntMethod {
  public:
    ////////////////////////////////////////////////////////////////////////////
    // INTMETHOD::~INTMETHOD
    // ---------------------
    // VIRTUAL DESTRUCTOR OF INTMETHOD CLASS
    //
    // IN:
    //   -
    // OUT:
    //   -
    ////////////////////////////////////////////////////////////////////////////
    virtual ~IntMethod(){};
    
    ////////////////////////////////////////////////////////////////////////////
    // INTMETHOD::INTEGRATE
    // --------------------
    // METHOD USED TO CALL FOR INTEGRATION
    //
    // IN:
    //   v (INTVISITOR&) ... VISITOR OBJECT DEFINING THE MODE OF INTEGRATION
    // OUT:
    //   -
    ////////////////////////////////////////////////////////////////////////////
    virtual void integrate(IntVisitor& v) {};
};

////////////////////////////////////////////////////////////////////////////////
// 
// INTMETHODTRAPEZ CLASS : INTMETHOD
// =================================
//
// CLASS DERIVED FROM INTMETHOD FOR USAGE OF TRAPEZOIDAL METHOD
//
////////////////////////////////////////////////////////////////////////////////
class IntMethodTrapez : public IntMethod {
  public:
    ////////////////////////////////////////////////////////////////////////////
    // INTMETHODTRAPEZ::INTEGRATE
    // --------------------------
    // METHOD USED TO CALL FOR INTEGRATION
    //
    // IN:
    //   v (INTVISITOR&) ... CALLS VISIT WITH ITSELF TO DEFINE 
    //                       INTEGRATION METHOD
    // OUT:
    //   -
    ////////////////////////////////////////////////////////////////////////////
    virtual void integrate(IntVisitor& v) {
      v.visit(*this);
    }
};

////////////////////////////////////////////////////////////////////////////////
// 
// INTMETHODSIMPSON13 CLASS : INTMETHOD
// ====================================
//
// CLASS DERIVED FROM INTMETHOD FOR USAGE OF SIMPSONS 1/3 METHOD
//
////////////////////////////////////////////////////////////////////////////////
class IntMethodSimpson13 : public IntMethod {
  public:
    ////////////////////////////////////////////////////////////////////////////
    // INTMETHODSIMPSON13::INTEGRATE
    // -----------------------------
    // METHOD USED TO CALL FOR INTEGRATION
    //
    // IN:
    //   v (INTVISITOR&) ... CALLS VISIT WITH ITSELF TO DEFINE 
    //                       INTEGRATION METHOD
    // OUT:
    //   -
    ////////////////////////////////////////////////////////////////////////////
    virtual void integrate(IntVisitor& v) {
      v.visit(*this);
    }
};

////////////////////////////////////////////////////////////////////////////////
// 
// INTMETHODGAUSS CLASS : INTMETHOD
// ================================
//
// CLASS DERIVED FROM INTMETHOD FOR USAGE OF GAUSS QUADRATURE IN [a, b]
//
////////////////////////////////////////////////////////////////////////////////
class IntMethodGauss : public IntMethod {
  public:
    ////////////////////////////////////////////////////////////////////////////
    // INTMETHODGAUSS::INTEGRATE
    // -------------------------
    // METHOD USED TO CALL FOR INTEGRATION
    //
    // IN:
    //   v (INTVISITOR&) ... CALLS VISIT WITH ITSELF TO DEFINE 
    //                       INTEGRATION METHOD
    // OUT:
    //   -
    ////////////////////////////////////////////////////////////////////////////
    virtual void integrate(IntVisitor& v) {
      v.visit(*this);
    }
};

////////////////////////////////////////////////////////////////////////////////
// 
// INTMETHODRAWGAUSS CLASS : INTMETHOD
// ===================================
//
// CLASS DERIVED FROM INTMETHOD FOR USAGE OF RAW GAUSS QUADRATURE IN [-1, 1]
//
////////////////////////////////////////////////////////////////////////////////
class IntMethodRawGauss : public IntMethod {
  public:
    ////////////////////////////////////////////////////////////////////////////
    // INTMETHODRAWGAUSS::INTEGRATE
    // ----------------------------
    // METHOD USED TO CALL FOR INTEGRATION
    //
    // IN:
    //   v (INTVISITOR&) ... CALLS VISIT WITH ITSELF TO DEFINE 
    //                       INTEGRATION METHOD
    // OUT:
    //   -
    ////////////////////////////////////////////////////////////////////////////
    virtual void integrate(IntVisitor& v) {
      v.visit(*this);
    }
};

////////////////////////////////////////////////////////////////////////////////
// 
// INTEGRATOR CLASS
// ================
//
// CORE CLASS OF THIS MODULE. CREATES AN OBJECT WHICH CAN INTEGRATE FUNCTIONS
// USAGE FOR INTEGRATING ({y}, h) USING TRAPEZOIDAL METHOD:
//
// Integrator<C2> i (IntegrationTechnique::trapez);
// C2 result = i.integrate(y, h);
//
// USAGE FOR INTEGRATING ({x}, f(x')) USING ORDER n GAUSS LEGENDRE:
//
// Integrator<C2> i (IntegrationTechnique::gauss);
// std::vector<C2> x_gauss (n);
// i.prepareGaussIntegration(x_gauss, x.front(), x.back());
// y_gauss = f(x_gauss);
// C2 result = i.integrate(x_gauss, y_gauss);
// 
////////////////////////////////////////////////////////////////////////////////
template<typename O2 = double, typename C2 = std::complex<O2>, typename OV2 = std::vector<O2>, typename CV2 = std::vector<C2>>
class Integrator {
  private:
    // INTEGRATION TECHNIQUE
    int technique;
    // RANGE OF INTEGRATION
    Range<O2> range;
    // VECTOR OF POINTERS TO ALL POSSIBLE INTEGRATORS
    std::vector<IntMethod*> integrators {
      new IntMethodTrapez(), 
      new IntMethodSimpson13(),
      new IntMethodGauss(),
      new IntMethodRawGauss()
    };
    // VISITOR FOR VISITOR POLYMORPHISM-TEMPLATE SCHEME 
    IntSubVisitor<O2, C2, OV2, CV2> args_visitor;
    bool parameters_have_changed = true;
    
  public:
    // POLYNOMIAL FOR GAUSS INTEGRATION TECHNIQUE
    SpecialPolynomial<O2>* polynomial_ptr;
    ////////////////////////////////////////////////////////////////////////////
    // INTEGRATOR::INTEGRATOR
    // ----------------------
    // CONSTRUCTOR FOR INTEGRATOR
    //
    // IN:
    //   technique_  (INTEGRATIONTECHNIQUE) ... TECHNIQUE USED FOR INTEGRATION
    // OUT:
    //   (INTEGRATOR)
    ////////////////////////////////////////////////////////////////////////////
    Integrator(IntegrationTechnique technique_) : 
        technique(static_cast<int>(technique_)) {
      // SET STANDARD POLYNOMIAL TO BE LEGENDRE POLYNOMIALS
      polynomial_ptr = &double_polynomial.Legendre;
    };
    
    ////////////////////////////////////////////////////////////////////////////
    // INTEGRATOR::INTEGRATE
    // ---------------------
    // INTEGRATES IN ({y}, h) MODE
    //
    // IN:
    //   y (CV2&) ... CONTAINER OF y VALUES
    //   h (C2)  ... x-SPACING OF y VALUES
    // OUT:
    //   (C2) RESULT OF INTEGRATION; VALUE OF INTEGRAL
    ////////////////////////////////////////////////////////////////////////////
    C2 integrate(const CV2 &y, O2 h) {
      // CREATE AN INTSUBVISITOR FOR GIVEN SETTINGS IF NOT ALREADY DONE
      if (!args_visitor.isValid() || this->parameters_have_changed)
        this->args_visitor = IntSubVisitor<O2, C2, OV2, CV2> (
            this->polynomial_ptr, this->range);
      // SET y AND h VALUES USED FOR INTEGRATION
      args_visitor.setData(y, h);
      // CALL INTEGRATE OF CORRESPONDING INTEGRATOR
      this->integrators[this->technique]->integrate(args_visitor);
      // FETCH AND RETURN RESULT OF INTEGRATION
      return args_visitor.getResult();
    };
    
    ////////////////////////////////////////////////////////////////////////////
    // INTEGRATOR::INTEGRATE
    // ---------------------
    // INTEGRATES IN ({x}, {y}) MODE
    //
    // IN:
    //   x     (CV2&)     ... CONTAINER OF x VALUES
    //   y     (CV2&)     ... CONTAINER OF y VALUES
    // OUT:
    //   (C2) RESULT OF INTEGRATION; VALUE OF INTEGRAL
    ////////////////////////////////////////////////////////////////////////////
    C2 integrate(const OV2 &x, const CV2 &y) {
      // CREATE AN INTSUBVISITOR FOR GIVEN SETTINGS IF NOT ALREADY DONE
      if (!args_visitor.isValid() || this->parameters_have_changed) {
        this->args_visitor = IntSubVisitor<O2, C2, OV2, CV2> (
            this->polynomial_ptr, this->range);
        this->parameters_have_changed = false;
      }
      // SET x AND y VALUES USED FOR INTEGRATION
      args_visitor.setData(x, y);
      // CALL INTEGRATE OF CORRESPONDING INTEGRATOR
      this->integrators[this->technique]->integrate(args_visitor);
      // FETCH AND RETURN RESULT OF INTEGRATION
      return args_visitor.getResult();
    };
    
    ////////////////////////////////////////////////////////////////////////////
    // INTEGRATOR::SETTECHNIQUE
    // ------------------------
    // SETS INTEGRATION TECHNIQUE
    // ONLY USED IF ONE WANTS TO CHANGE THE VARIANT SET IN THE CONSTRUCTOR
    //
    // IN:
    //   technique_ (INTEGRATIONTECHNIQUE) ... ENUM ELEMENT OF TECHNIQUE
    // OUT:
    //   -
    ////////////////////////////////////////////////////////////////////////////
    void setTechnique(IntegrationTechnique technique_) {
        this->technique = static_cast<int>(technique_);
    };
    
    ////////////////////////////////////////////////////////////////////////////
    // INTEGRATOR::SETRANGE
    // --------------------
    // SETS INTEGRATION RANGE
    //
    // IN:
    //   from (O2) ... LOWER BOUND OF INTEGRATION
    //   to   (O2) ... UPPER BOUND OF INTEGRATION
    // OUT:
    //   -
    ////////////////////////////////////////////////////////////////////////////
    void setRange(O2 from, O2 to) {
        this->Range = Range(from, to);
        this->parameters_have_changed = true;
    };

    ////////////////////////////////////////////////////////////////////////////
    // INTEGRATOR::SETPOLYNOMIAL
    // -------------------------
    // SETS THE POLYNOMIAL TO USE WITH GAUSS INTEGRATION TECHNIQUE
    //
    // IN:
    //   p_ptr (SPECIALPOLYNOMIAL<C>*) ... POINTER TO POLYNOMIAL OBJECT
    // OUT:
    //   -
    ////////////////////////////////////////////////////////////////////////////
    void setPolynomial(SpecialPolynomial<O2>* p_ptr) {
      this->polynomial_ptr = p_ptr;
      // RELOAD PARAMETERS WHEN INTEGRATION CALLED NEXT TIME
      this->parameters_have_changed = true;
    };
    
    ////////////////////////////////////////////////////////////////////////////
    // INTEGRATOR::PREPAREGAUSSINTERPOLATION
    // -------------------------------------
    // PREPARES A FULL GAUSS INTEGRATION
    // RETRIEVES X VALUES AS ZEROS OF SET POLYNOMIALS
    // ORDER IS DEDUCED BY SIZE OF x_gauss CONTAINER
    //
    // IN:
    //   x_gauss (CV) ... REFERENCE TO EMPTY VECTOR FOR STORAGE OF X
    //   from    (C) ... LOWER BOUND OF INTEGRATION
    //   to      (C) ... UPPER BOUND OF INTEGRATION
    // OUT:
    //   -
    ////////////////////////////////////////////////////////////////////////////
    void prepareGaussIntegration(OV2 &x_gauss, O2 from, O2 to) {
      // SET ORDER TO BE OF LENGTH OF x_gauss_legendre CONTAINER
      int n = (int) (x_gauss.end() - x_gauss.begin());
      // CREATE RANGE OBJECT
      this->range = Range<O2>(from, to);
      // GET ROOTS OF LEGENDRE POLYNOMIALS IN [-1, 1]
      std::vector<O2> x_gauss_normalized = polynomial_ptr->getRoots(n);
      // TRANSFORM ZEROS FROM [-1, 1] TO [from, to]
      LogarithmicTransformer<O2> tr(from, to);
      for (int j = 0; j < n; j++)
        x_gauss[j] = tr(x_gauss_normalized[j]);
      // RELOAD PARAMETERS WHEN INTEGRATION CALLED NEXT TIME
      this->parameters_have_changed = true;
    };
};

#endif /* INTEGRATOR_H */
