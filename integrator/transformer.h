////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////
////  TRANSFORMER.H
////  =============
////
////  HEADER FILE CONTAINING TOOLS TO HANDLE MATHEMATICAL VARIABLE 
////  TRANSFORMATIONS.
////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifndef TRANSFORMER_H
#define TRANSFORMER_H

#include <cmath>
using std::pow;
using std::sqrt;
using std::log;

////////////////////////////////////////////////////////////////////////////////
// 
// TRANSFORMER CLASS
// =================
//
// ABSTRACT TRANSFORMER CLASS FOR POINTER CREATION TO CONCRETE TRANSFORMATION
//
////////////////////////////////////////////////////////////////////////////////
template<typename T>
class Transformer {
	public:
		// CONCRETE TRANSFORMER MUST IMPLEMENT TRANSFORM METHOD
		virtual T transform(const T x) const = 0;
		virtual std::vector<T> transform(const std::vector<T>& x) const = 0;
		// AND INVERSETRANSFORM METHOD
		virtual T inverseTransform(const T x) const = 0;
		virtual std::vector<T> inverseTransform(const std::vector<T>& x)
				const = 0;
		// METHOD GETTING THE JACOBIAN OF A GIVEN TRANSFORM
		virtual T getJacobian(const T x, const bool transformed = false) 
				const = 0;
		virtual std::vector<T> getJacobian(const std::vector<T>& x, 
				const bool transformed = false) const = 0;
};

////////////////////////////////////////////////////////////////////////////////
// 
// LOGARITHMICTRANSFORMER CLASS
// ============================
//
// CLASS USED FOR TRANSFORMING AN ARBITRARY INTERVAL [A, B] LOGARITHMICALLY
// TO THE NORMALIZED [-1, 1] INTERVAL.
// USAGE FOR TRANSFORMING xi FROM [a, b] TO [-1, 1]:
// 
// Transformer<T> tr (a, b);
// T transformed_xi = tr(xi);
//
////////////////////////////////////////////////////////////////////////////////
template<typename T = double>
class LogarithmicTransformer : public Transformer<T> {
	private:
		// CONSTANTS USED FOR CONVERSION
		T A;
		T B;
		// SMALL NUMBER USED FOR COMPARISONS AND LIMITS
		static T TOLERANCE;
	
	public:
		////////////////////////////////////////////////////////////////////////
		// LOGARITHMICTRANSFORMER::LOGARITHMICTRANSFORMER
		// ----------------------------------------------
		// CONSTRUCTOR FOR A TRANSFORMER
		//
		// IN: 
		//   a (T) ... LOWER BOUND
		//   b (T) ... UPPER BOUND
		// OUT:
		//   (TRANSFORMER)
		////////////////////////////////////////////////////////////////////////
		LogarithmicTransformer(T a, T b) {
			// CHECK FOR INVALID VALUES AND CORRECT IF POSSIBLE
			if(a * b < -LogarithmicTransformer::TOLERANCE)
				throw "Integrating over 0 not allowed";
			if(std::abs(a / b - 1) < LogarithmicTransformer::TOLERANCE)
				throw "Integration range too small";
			if (std::abs(a) < LogarithmicTransformer::TOLERANCE)
				a = LogarithmicTransformer::TOLERANCE;
			if (std::abs(b) < LogarithmicTransformer::TOLERANCE)
				b = LogarithmicTransformer::TOLERANCE;
			// CALCULATE A AND B FROM a AND b
			//this->A = - log(a * b) / log(b / a);
			//this->B = 2 / log(b / a);
			this->A = a;
			this->B = b;
		};
		
		////////////////////////////////////////////////////////////////////////
		// LOGARITHMICTRANSFORMER::INVERSETRANSFORM
		// ----------------------------------------
		// TRANSFORMS A POINT FROM THE RANGE [a, b] TO THE RANGE [-1, 1]
		//
		// IN:
		//   x (T) ... POINT IN [a, b] TO TRANSFORM TO [-1, 1]
		// OUT:
		//   (T) INVERSELY TRANSFORMED x IN [-1, 1]
		////////////////////////////////////////////////////////////////////////
		T inverseTransform(const T x) const {
			//return this->B * log(x) + this->A;
			return (2.0 * logf(x) - logf(A * B)) / logf(B / A);
		};
		
		////////////////////////////////////////////////////////////////////////
		// LOGARITHMICTRANSFORMER::INVERSETRANSFORM
		// ----------------------------------------
		// TRANSFORMS A VECTOR FROM THE RANGE [a, b] TO THE RANGE [-1, 1]
		//
		// IN:
		//   x_vec (STD::VECTOR<T>&) ... VECTOR IN [a, b] TO TRANSFORM TO 
		//								 [-1, 1]
		// OUT:
		//   (STD::VECTOR<T>) INVERSELY TRANSFORMED x_vec IN [-1, 1]
		////////////////////////////////////////////////////////////////////////
		std::vector<T> inverseTransform(const std::vector<T>& x_vec) const {
			std::vector<T> x_tr;
			for(auto& x : x_vec)
				x_tr.push_back(inverseTransform(x));
			return x_tr;
		};
		
		////////////////////////////////////////////////////////////////////////
		// LOGARITHMICTRANSFORMER::TRANSFORM
		// ---------------------------------
		// TRANSFORMS A POINT FROM THE RANGE [-1, 1] TO THE RANGE [a, b]
		//
		// IN:
		//   x (T) ... POINT IN [-1, 1] TO TRANSFORM TO [a, b]
		// OUT:
		//   (T) TRANSFORMED x IN [a, b]
		////////////////////////////////////////////////////////////////////////
		T transform(const T x) const {
			//return exp((x - this->A) / this->B);
			return sqrt(A * B) * pow(sqrt(B) / sqrt(A), x);
		};
				
		////////////////////////////////////////////////////////////////////////
		// LOGARITHMICTRANSFORMER::TRANSFORM
		// ---------------------------------
		// TRANSFORMS A VECTOR FROM THE RANGE [-1, 1] TO THE RANGE [a, b]
		//
		// IN:
		//   x_vec (STD::VECTOR<T>&) ... VECTOR IN [-1, 1] TO TRANSFORM TO 
		//								 [a, b]
		// OUT:
		//   (STD::VECTOR<T>) TRANSFORMED x_vec IN [a, b]
		////////////////////////////////////////////////////////////////////////
		std::vector<T> transform(const std::vector<T>& x_vec) const {
			std::vector<T> x_tr;
			for(auto& x : x_vec)
				x_tr.push_back(transform(x));
			return x_tr;
		};
		
		////////////////////////////////////////////////////////////////////////
		// LOGARITHMICTRANSFORMER::OPERATOR()
		// ----------------------------------
		// OVERLOADED FUNCTION CALL OPERATOR TO BE USED FOR TRANSFORMING
		//
		// IN:
		//   x (T) ... POINT IN [a, b] TO TRANSFORM TO [-1, 1]
		// OUT:
		//   (T) TRANSFORMED x IN [-1, 1]
		////////////////////////////////////////////////////////////////////////
		T operator() (const T x) const {
			return this->transform(x);
		};
		
		////////////////////////////////////////////////////////////////////////
		// LOGARITHMICTRANSFORMER::OPERATOR()
		// ----------------------------------
		// OVERLOADED FUNCTION CALL OPERATOR TO BE USED FOR TRANSFORMING
		//
		// IN:
		//   x (STD::VECTOR<T>&) ... VECTOR OF POINTS IN [a, b] TO TRANSFORM 
		//                           TO [-1, 1]
		// OUT:
		//   (STD::VECTOR<T>) VECTOR OF TRANSFORMED x IN [-1, 1]
		////////////////////////////////////////////////////////////////////////
		std::vector<T> operator() (const std::vector<T> &x) const {
			std::vector<T> x_transformed;
			for (auto& xi : x)
				x_transformed.push_back(this->transform(xi));
			return x_transformed;
		};
		
		////////////////////////////////////////////////////////////////////////
		// LOGARITHMICTRANSFORMER::GETJACOBIAN
		// -----------------------------------
		// CALCULATES THE JACOBIAN FOR THIS TRANSFORMATION
		//
		// IN:
		//   x                   (T)    ... x DEPENDENCY OF THE JACOBIAN
		//   already_transformed (BOOL) ... WHETHER x IS TRANSFORMED INTO 
		//                                  RANGE [-1, 1]
		// OUT:
		//   (T) JACOBIAN OF THIS TRANSFORMATION
		////////////////////////////////////////////////////////////////////////
		T getJacobian(const T x, const bool already_transformed = false) const {
			if (already_transformed)
				//return x / this->B;
				return x * logf(B / A) * 0.5;
			else
				return this->transform(x) * logf(B / A) * 0.5;
		};
		
		////////////////////////////////////////////////////////////////////////
		// LOGARITHMICTRANSFORMER::GETJACOBIAN
		// -----------------------------------
		// CALCULATES THE JACOBIAN FOR THIS TRANSFORMATION
		//
		// IN:
		//   x_vec               (STD::VECTOR<T>&)  ... x DEPENDENCY OF JACOBIAN
		//   already_transformed (BOOL)       ... WHETHER x IS NORMALIZED 
		//										  TO [-1, 1]
		// OUT:
		//   (STD::VECTOR<T>) JACOBIAN OF THIS TRANSFORMATION
		////////////////////////////////////////////////////////////////////////
		std::vector<T> getJacobian(const std::vector<T>& x_vec, 
				const bool already_transformed = false) const {
			std::vector<T> j;
			for (auto& x : x_vec) {
				/* if (already_transformed)
					j.push_back(x / this->B);
				else
					j.push_back(this->transform(x) / this->B);*/
				j.push_back(getJacobian(x, already_transformed));
			}
			return j;
		};
		
		////////////////////////////////////////////////////////////////////////
		// LOGARITHMICTRANSFORMER::PRINTCONSTANTS
		// --------------------------------------
		// PRINT METHOD USED FOR DEBUGGING. PRINTS A AND P TO STDOUT
		//
		// IN:
		//   -
		// OUT:
		//   -
		////////////////////////////////////////////////////////////////////////
		void printConstants() const {
			std::cout << " (A, B) = (" << this->A << ", " << this->B << ")" 
					<< std::endl;
		};
};

// SET DEFAULT VALUE FOR TOLERANCE OF TRANSFORMER
template<typename T>
T LogarithmicTransformer<T>::TOLERANCE = 1e-12;

////////////////////////////////////////////////////////////////////////////////
// 
// LINEARTRANSFORMER CLASS
// =======================
//
// CLASS USED FOR TRANSFORMING AN ARBITRARY INTERVAL [A, B] LINEARLY
// TO THE NORMALIZED [-1, 1] INTERVAL.
// USAGE FOR TRANSFORMING xi FROM [a, b] TO [-1, 1]:
// 
// LinearTransformer<T> tr (a, b);
// T transformed_xi = tr(xi);
//
////////////////////////////////////////////////////////////////////////////////
template<typename T = double>
class LinearTransformer : public Transformer<T> {  
	private:
		T a;
		T b;
	
	public:
		////////////////////////////////////////////////////////////////////////
		// LINEARTRANSFORMER::LINEARTRANSFORMER
		// ------------------------------------
		// CONSTRUCTOR FOR A LINEAR TRANSFORMER
		//
		// IN: 
		//   a (T) ... LOWER BOUND
		//   b (T) ... UPPER BOUND
		// OUT:
		//   (TRANSFORMER)
		////////////////////////////////////////////////////////////////////////
		LinearTransformer(const T a_, const T b_) {
			assert(a_ < b_);
			this->a = a_;
			this->b = b_;
		};
		
		////////////////////////////////////////////////////////////////////////
		// LINEARTRANSFORMER::INVERSETRANSFORM
		// -----------------------------------
		// TRANSFORMS A POINT FROM THE RANGE [-1, 1] TO THE RANGE [a, b]
		//
		// IN:
		//   x (T) ... POINT IN [-1, 1] TO TRANSFORM TO [a, b]
		// OUT:
		//   (T) INVERSELY TRANSFORMED x IN [a, b]
		////////////////////////////////////////////////////////////////////////
		T inverseTransform(const T x) const {
			return ((b - a) * x + a + b) / 2;
		};
		
		////////////////////////////////////////////////////////////////////////
		// LINEARTRANSFORMER::INVERSETRANSFORM
		// -----------------------------------
		// TRANSFORMS A VECTOR FROM THE RANGE [-1, 1] TO THE RANGE [a, b]
		//
		// IN:
		//   x_vec (STD::VECTOR<T>&) ... VECTOR IN [-1, 1] TO TRANSFORM TO 
		//								 [a, b]
		// OUT:
		//   (STD::VECTOR<T>) INVERSELY TRANSFORMED x_vec IN [a, b]
		////////////////////////////////////////////////////////////////////////
		std::vector<T> inverseTransform(const std::vector<T>& x_vec) const {
			std::vector<T> res;
			for (auto& x : x_vec)
				res.push_back(this->inverseTransform(x));
			return res;
		};
		
		////////////////////////////////////////////////////////////////////////
		// LINEARTRANSFORMER::TRANSFORM
		// ----------------------------
		// TRANSFORMS A POINT FROM THE RANGE [a, b] TO THE RANGE [-1, 1]
		//
		// IN:
		//   z (T) ... POINT IN [a, b] TO TRANSFORM TO [-1, 1]
		// OUT:
		//   (T) TRANSFORMED z IN [-1, 1]
		////////////////////////////////////////////////////////////////////////
		T transform(const T z) const {
			return (2 * z - a - b) / (b - a);
		};

		////////////////////////////////////////////////////////////////////////
		// LINEARTRANSFORMER::TRANSFORM
		// ----------------------------
		// TRANSFORMS A VECTOR FROM THE RANGE [a, b] TO THE RANGE [-1, 1]
		//
		// IN:
		//   z (STD::VECTOR<T>&) ... VECTOR IN [a, b] TO TRANSFORM TO [-1, 1]
		// OUT:
		//   (STD::VECTOR<T>) TRANSFORMED z_vec IN [-1, 1]
		////////////////////////////////////////////////////////////////////////
		std::vector<T> transform(const std::vector<T>& x_vec) const {
			std::vector<T> res;
			for (auto& x : x_vec)
				res.push_back(this->transform(x));
			return res;
		};
		
		////////////////////////////////////////////////////////////////////////
		// LINEARTRANSFORMER::OPERATOR()
		// -----------------------------
		// OVERLOADED FUNCTION CALL OPERATOR TO BE USED FOR TRANSFORMING
		//
		// IN:
		//   x (T) ... POINT IN [a, b] TO TRANSFORM TO [-1, 1]
		// OUT:
		//   (T) TRANSFORMED x IN [-1, 1]
		////////////////////////////////////////////////////////////////////////
		T operator() (const T x) const {
			return this->transform(x);
		};
		
		////////////////////////////////////////////////////////////////////////
		// LINEARTRANSFORMER::OPERATOR()
		// -----------------------------
		// OVERLOADED FUNCTION CALL OPERATOR TO BE USED FOR TRANSFORMING
		//
		// IN:
		//   x (STD::VECTOR<T>&) ... VECTOR OF POINTS IN [a, b] TO TRANSFORM 
		//                           TO [-1, 1]
		// OUT:
		//   (STD::VECTOR<T>) VECTOR OF TRANSFORMED x IN [-1, 1]
		////////////////////////////////////////////////////////////////////////
		std::vector<T> operator() (const std::vector<T> &x) const {
			std::vector<T> x_transformed;
			for (auto& xi : x)
				x_transformed.push_back(this->transform(xi));
			return x_transformed;
		};
		
		////////////////////////////////////////////////////////////////////////
		// LINEARTRANSFORMER::GETJACOBIAN
		// ------------------------------
		// CALCULATES THE JACOBIAN FOR THIS TRANSFORMATION
		//
		// IN:
		//   _unused_    (T)    ... ARBITRARY VALUE
		//   transformed (BOOL) ... ARBITRARY BOOL
		// OUT:
		//   (T) JACOBIAN OF THIS TRANSFORMATION
		////////////////////////////////////////////////////////////////////////
		T getJacobian(const T _unused_, const bool transformed = false) const {
			return (b - a) / 2;
		};

		////////////////////////////////////////////////////////////////////////
		// LINEARTRANSFORMER::GETJACOBIAN
		// ------------------------------
		// CALCULATES THE JACOBIAN FOR THIS TRANSFORMATION
		//
		// IN:
		//   _unused_    (STD::VECTOR<T>&) ... ARBITRARY VECTOR
		//   transformed (BOOL)            ... ARBITRARY BOOL
		// OUT:
		//   (STD::VECTOR<T>) JACOBIAN OF THIS TRANSFORMATION
		////////////////////////////////////////////////////////////////////////
		std::vector<T> getJacobian(const std::vector<T>& _unused_, 
				const bool transformed = false) const {
			return std::vector<T>(_unused_.size(), (b - a) / 2);
		};
};

#endif /* TRANSFORMER_H */