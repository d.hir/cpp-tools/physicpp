////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////
////  PLOTUTILS.H
////  ===========
////
////  HEADER FILE CONTAINING A MATLAB-ESK INTERFACE FOR GNUPLOT.
////
////  THIS USES AN EXTERNAL LIBRARY "gnuplot-iostream.h" FOR ACCESSING GNUPLOT.
////  IT IS AVAILABLE AT GITHUB: https://github.com/dstahlke/gnuplot-iostream
////  THE AUTHOR WANTS THE FOLLOWING COPYRIGHT NOTICE TO BE ADDED:
////  THIS COPYRIGHT NOTICE ONLY CONCERNS gnuplot-iostream.h!
//
// Copyright (c) 2020 Daniel Stahlke (dan@stahlke.org)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifndef PLOTUTILS_H
#define PLOTUTILS_H

/* FOR ERROR AND DEBUG MESSAGES */
#include <iostream>
/* STANDARD CONTAINER USED IN THIS MODULE */
#include <vector>
/* FOR USING E.G. SQRT */
#include <cmath>
/* USING TUPLES IN (x, y) PLOTS */
#include <boost/tuple/tuple.hpp>
/* STRINGS AS E.G. TITLES OF PLOTS */
#include <string>
/* USED FOR FORMATTING STRINGS */
#include <boost/format.hpp>

/* EXTERNAL LIBRARY AS INTERFACE TO GNUPLOT */
#include "gnuplot-iostream.h"

/* WRAP EVERYTHING INTO ITS OWN NAMESPACE ("plotutils") */
namespace plotutils {

////////////////////////////////////////////////////////////////////////////////
// 
// FIGURE CLASS
// ============
//
// CLASS NEEDED TO PLOT DATA. DESCRIBES A GRAPHICAL FIGURE (A "WINDOW").
// USAGE TO PLOT ({x}, {y}) DATA SETS OF TYPE T:
//
// Figure<T> fig;
// fig.plot(x, y);
//
// TO PLOT LOGARITHMICALLY ADD A STRING AS A CONSTRUCTOR ARGUMENT DESCRIBING
// THE AXIS (AXES) WHICH SHOULD BE DISPLAYED LOGARITHMICALLY.
//
// E.G. FOR SEMILOGARITHMIC x PLOT:
// Figure<T> fig ("x");
// E.G. FOR DOUBLELOGARITHMIC PLOT:
// Figure<T> fig ("xy");
//
////////////////////////////////////////////////////////////////////////////////
template<typename T = double>
class Figure {
private:
    /* GNUPLOT OBJECT PROVIDED BY gnuplot-iostream.h HEADER */
    Gnuplot gp;
    /* WHETHER THE FOLLOWING PLOT IS THE FIRST ONE IN THIS FIGURE */
    bool first_plot;
    /* WHETHER TO DISPLAY THE KEY (LEGEND) OR NOT */
    bool show_key;
    /* WHETHER THE FIGURE IS VALID OR NOT */
    bool is_valid;

    ////////////////////////////////////////////////////////////////////////////
    // FIGURE::_RAW_PLOT
    // -----------------
    // PRIVATE METHOD PLOTTING DATA WITHOUT ANY INPUT HANDLING
    //
    // IN:
    //   x          (V&)     ... INPUT AS ARBITRARY DATA STRUCTURE
    //   data_title (STRING) ... TEXT DISPLAYED IN LEGEND
    //   witharg    (STRING) ... ADDITIONAL ARGUMENTS TO GNUPLOT
    // OUT:
    //   -
    ////////////////////////////////////////////////////////////////////////////
    template<typename V>
    void _raw_plot(V& x, std::string& data_title, std::string& witharg) {
        /* IF THIS IS NOT THE FIRST PLOT, PLOT AS 'replot' (NOT 'plot') */
        if (!this->first_plot)
            gp << "re";
        /* PLOT DATA SUPPLIED AS x */
        gp << "plot" << gp.file1d(x);
        /* IF A TITLE IS SUPPLIED, TELL IT GNUPLOT */
        if (data_title != "") {
            gp << " title \"" << data_title << "\"";
            /* IF A TITLE IS SUPPLIED, OBVIOUSLY SHOW IT */
            this->show_key = true;
        }
        /* IF ANY ADDITIONAL ARGUMENT IS SUPPLIED, TELL IT GNUPLOT */
        if (witharg != "")
            gp << " " << witharg;
        /* SEND COMMAND TO GNUPLOT */
        gp << "\n";
        /* IF AT LEAST ONE GRAPH TITLE IS SUPPLIED, SHOW THE LEGEND */
        if (this->show_key)
            gp << "set key on\n";
        /* NOW THE FOLLOWING PLOT IS NOT THE FIRST ANYMORE */
        this->first_plot = false;
    }

public:
    ////////////////////////////////////////////////////////////////////////////
    // FIGURE::FIGURE
    // --------------
    // CONSTRUCTOR CREATING A FIGURE
    //
    // IN:
    //   logmode  (STRING) ... STRING DESCRIBING IF PLOT SHOULD BE LOGARITHMIC
    //                         POSSIBLE VALUES: "x", "y", "xy"
    //   validate (BOOL)   ... IF FIGURE SHOULD BE DECLARED VALID 
    //                         ONLY SET TO false IF YOU KNOW WHAT YOU'RE DOING!
    // OUT:
    //   (FIGURE)
    ////////////////////////////////////////////////////////////////////////////
    Figure(std::string logmode = "", bool validate = true) : 
            /* THE FOLLOWING PLOT IS OBVIOUSLY THE FIRST ONE IN THIS FIGURE */
            first_plot(true), 
            /* BY DEFAULT, DO NOT SHOW LEGEND (INTERNAL VARIABLE)*/
            /* AS SOON AS ONE GRAPH NAME IS SUPPLIED, IT WILL BE SHOWN */
            show_key(false), 
            /* SET VALIDITY */
            is_valid(validate) {
        /* BY DEFAULT DO NOT SHOW LEGEND (SEND TO GNUPLOT) */
        gp << "set key off\n";
        /* SET LOGMODE */
        if (logmode != "")
            gp << "set logscale " << logmode << "\n";
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // FIGURE::ISVALID
    // ---------------
    // CHECK WHETHER THIS FIGURE IS VALID
    //
    // IN:
    //   -
    // OUT:
    //   (BOOL) VALIDITY OF THIS FIGURE
    ////////////////////////////////////////////////////////////////////////////
    bool isValid() {
        return this->is_valid;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // FIGURE::PLOT
    // ------------
    // METHOD WHICH SHOULD BE CALLED TO PLOT DATA ONLY GIVEN BY A SINGLE
    // DATA VECTOR (x AXIS WILL BE TAKEN AS NATURAL NUMBERS)
    //
    // IN:
    //   x          (VECTOR<T>&) ... VECTOR CONTAINING VALUES TO PLOT
    //   data_title (STRING)     ... TEXT TO BE DISPLAYED IN LEGEND
    //   witharg    (STRING)     ... ADDITIONAL ARGUMENTS FOR GNUPLOT
    // OUT:
    //   -
    ////////////////////////////////////////////////////////////////////////////
    void plot(const std::vector<T> &x, std::string data_title = "", 
            std::string witharg = "") {
        _raw_plot(x, data_title, witharg);
    }

    ////////////////////////////////////////////////////////////////////////////
    // FIGURE::PLOT
    // ------------
    // METHOD WHICH SHOULD BE CALLED TO PLOT DATA GIVEN AS A x AND y VECTOR
    //
    // IN:
    //   x          (VECTOR<T>&) ... VECTOR CONTAINING x VALUES TO PLOT
    //   y          (VECTOR<T>&) ... VECTOR CONTAINING y VALUES TO PLOT
    //   data_title (STRING)     ... TEXT TO BE DISPLAYED IN LEGEND
    //   witharg    (STRING)     ... ADDITIONAL ARGUMENTS FOR GNUPLOT
    // OUT:
    //   -
    ////////////////////////////////////////////////////////////////////////////
    template<typename U>
    void plot(const std::vector<T> &x, std::vector<U> &y, 
            std::string data_title = "", std::string witharg = "") {
        /* MASH VECTORS INTO A SET OF (x, y) PAIRS */
        auto tpl = boost::make_tuple(x, y);
        _raw_plot(tpl, data_title, witharg);
    }

    ////////////////////////////////////////////////////////////////////////////
    // FIGURE::_SET
    // ------------
    // METHOD WHICH SETS A GIVEN KEY TO A GIVEN VALUE
    //
    // IN:
    //   key         (STRING) ... KEY TO CHANGE
    //   value       (STRING) ... VALUE TO CHANGE KEY INTO
    //   delim_left  (STRING) ... LEFT DELIMITER ADDED TO VALUE
    //   delim_right (STRING) ... RIGHT DELIMITER ADDED TO VALUE
    // OUT:
    //   -
    ////////////////////////////////////////////////////////////////////////////
    void _set(std::string key, std::string value,
            std::string delim_left = "\"", std::string delim_right = "\"") {
        /* SET KEY TO VALUE */
        gp << "set " << key << " " << delim_left << value << delim_right 
                << "\n";
        /* REPLOT AFTERWARDS TO APPLY CHANGES */
        gp << "replot\n";
    }

    ////////////////////////////////////////////////////////////////////////////
    // FIGURE::TITLE
    // -------------
    // METHOD WHICH SETS THE TITLE OF THE WHOLE FIGURE
    //
    // IN:
    //   title_str (STRING) ... STRING WHICH GETS SET AS THE FIGURE SUPERTITLE
    // OUT:
    //   -
    ////////////////////////////////////////////////////////////////////////////
    void title(std::string title_str) {
        _set("title", title_str);
    }

    ////////////////////////////////////////////////////////////////////////////
    // FIGURE::XLABEL
    // --------------
    // METHOD WHICH SETS THE LABEL OF THE x AXIS
    //
    // IN:
    //   label_str (STRING) ... STRING WHICH GETS SET AS THE LABEL OF THE x AXIS
    // OUT:
    //   -
    ////////////////////////////////////////////////////////////////////////////
    void xlabel(std::string label_str) {
        _set("xlabel", label_str);
    }

    ////////////////////////////////////////////////////////////////////////////
    // FIGURE::YLABEL
    // --------------
    // METHOD WHICH SETS THE LABEL OF THE y AXIS
    //
    // IN:
    //   label_str (STRING) ... STRING WHICH GETS SET AS THE LABEL OF THE y AXIS
    // OUT:
    //   -
    ////////////////////////////////////////////////////////////////////////////
    void ylabel(std::string label_str) {
        _set("ylabel", label_str);
    }

    ////////////////////////////////////////////////////////////////////////////
    // FIGURE::RAW_CMD
    // ---------------
    // METHOD WHICH EXECUTES A GIVEN COMMAND DIRECTLY
    //
    // IN:
    //   cmd    (STRING) ... COMMAND WHICH SHOULD BE EXECUTED
    //   replot (BOOL)   ... WHETHER TO REPLOT AFTER EXECUTING COMMAND
    // OUT:
    //   -
    ////////////////////////////////////////////////////////////////////////////
    void raw_cmd(std::string cmd, bool replot = true) {
        /* WRITE COMMAND */
        gp << cmd;
        /* SEND NEWLINE CHARACTER IF cmd DOES NOT ALREADY END IN ONE */
        if (cmd.back() != '\n')
            gp << '\n';
        /* REPLOT IF ASKED TO */
        if (replot)
            gp << "replot\n";
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // FIGURE::OVERRIDEWITHNEXTPLOT
    // ----------------------------
    // IF CALLED, THE NEXT PLOT COMMAND WILL OVERRIDE ALL PREVIOUS PLOT 
    // COMMANDS. ALL GRAPHS ALREADY PRESENT IN THE CURRENT FIGURE WILL VANISH.
    //
    // IN:
    //   -
    // OUT:
    //   -
    ////////////////////////////////////////////////////////////////////////////
    void overrideWithNextPlot() {
        this->first_plot = true;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // FIGURE::FLUSH
    // -------------
    // EXECUTE EVERYTHING IN BUFFER RIGHT NOW.
    //
    // IN:
    //   -
    // OUT:
    //   -
    ////////////////////////////////////////////////////////////////////////////
    void flush() {
        gp.do_flush();
    }
};

// ############################ HISTOGRAM CLASS ############################ //

/* SMALL CONSTANT DESCRIBING ERROR TOLERANCE IN PLOTUTILS (PU) NAMESPACE */
#define PU_TOLERANCE 0.01

////////////////////////////////////////////////////////////////////////////////
// 
// HISTOGRAM CLASS
// ============
//
// CLASS ABLE TO PLOT HISTOGRAMS OF A GIVEN DATA SET. 
// USAGE TO PLOT A DATA SET x IN 10 BARS FROM 0.0 TO 5.0 WITH BAYESIAN
// UNCERATAINTY BARS:
//
// Histogram<T> h (10, 0.0, 5.0);
// h.setData(x);
// h.plot(3);
//
// TODO: MAKE HISTOGRAM CLASS INHERIT FROM FIGURE CLASS TO PREVENT DOUBLE
//       DEFINITION OF SOME METHODS LIKE E.G. 'TITLE'. 
//
// THIS CLASS IS KIND OF A MESS. IT NEEDS LOTS OF REFACTORING TO BE ACCEPTABLE.
// IT WORKS (KIND OF), BUT SHOULD NOT STAY AS IT CURRENTLY IS FOREVER.
//
////////////////////////////////////////////////////////////////////////////////
class Histogram {
    private:
        /* NUMBER OF BINS TO PLOT */
        int num_bins;
        /* WIDTH OF EACH BIN */
        std::vector<double> bin_width;
        /* NUMBER OF ELEMENTS IN EACH BIN */
        std::vector<int> bin;
        /* MINIMUM VALUE (LOWEST BIN) */
        double min;
        /* MAXIMUM VALUE (HIGHEST BIN) */
        double max;
        /* BIN CONTAINING ALL ELEMENTS OUTSIDE [min, max] */
        int remainder_bin;
        /* Gnuplot OBJECT FROM gnuplot-iostream.h HEADER */
        Gnuplot gp;
        /* LENGTH OF INTERVAL TO PLOT */
        double range;
        /* VECTOR CONTAINING THE UPPER LIMIT OF EACH BIN */
        std::vector<double> maxvals;
        /* VARIABLE TRACKING TOTAL NUMBER OF ELEMENTS TO CLASSIFY */
        int total_elements;
        
        /* SETS THE VECTOR MAXVALS ACCORDING TO ARGUMENTS OF CONSTRUCTOR */
        void setMaxVals();
        /* CALCULATE UNCERTAINTY USING FREQUENTIST APPROACH */
        std::vector<double> frequentistUncertainty();
        /* CALCULATE UNCERTAINTY USING BAYESIAN APPROACH */
        std::vector<double> bayesianUncertainty(std::vector<double> avg_pi);
        /* RETURNS TICKS OF X-AXIS */
        std::vector<std::string> getXTicks(int num_ticks);
        
    public:
        /* NORMAL (ONLY) CONSTRUCTOR OF HISTOGRAM CLASS */
        Histogram(int num_bins_, double min_, double max_);
        /* SETS THE WIDTH OF EACH BIN MANUALLY */
        int setIndividualWidth(std::vector<double> bin_width_);
        /* SETS THE DATA TO PLOT */
        void setData(std::vector<double> data);
        /* PLOTS DATA USING A GIVEN VARIANT */
        void plot(int variant);
        /* PLOTS DATA USING A GIVEN VARIANT AND ALSO PLOTS lineplotstring */
        void plot(int variant, std::string lineplotstring);
        /* PRINTS THE BIN FULLNESS TO STDOUT */
        void printBins();
        /* SETS A SPECIFIC KEY TO A SPECIFIC VALUE */
        void _set(std::string key, std::string value,
                std::string delim_left, std::string delim_right);
        /* SETS LABEL OF X AXIS */
        void xlabel(std::string label_str);
        /* SETS LABEL OF Y AXIS */
        void ylabel(std::string label_str);
        /* SETS SUPERTITLE OF WHOLE HISTOGRAM */
        void title(std::string title_str);
};

////////////////////////////////////////////////////////////////////////////////
// HISTOGRAM::HISTOGRAM
// --------------------
// NORMAL (ONLY) CONSTRUCTOR FOR HISTOGRAM CLASS
//
// IN:
//   num_bins_ (INT)    ... NUMBER OF BINS TO PLOT
//   min_      (DOUBLE) ... MINIMUM VALUE TO PLOT
//   max_      (DOUBLE) ... MAXIMUM VALUE TO PLOT
// OUT:
//   (HISTOGRAM)
////////////////////////////////////////////////////////////////////////////////
Histogram::Histogram(int num_bins_, double min_, double max_) {
    /* SET NUMBER OF BINS */
    this->num_bins = num_bins_;
    /* SET MINIMUM */
    this->min = min_;
    /* SET MAXIMUM */
    this->max = max_;
    /* SET RANGE */
    this->range = this->max - this->min;
    /* SET BIN WIDTH TO BE CONSTANT */
    this->bin_width = *(new 
            std::vector<double>(this->num_bins, this->range / this->num_bins));
    /* SET BINS TO ALL BE EMPTY */
    this->bin = *(new std::vector<int> (this->num_bins, 0));
    /* SET TOTAL NUMBER OF ELEMENTS TO 0 */
    this->total_elements = 0;
    /* CLEAR REMAINDER BIN */
    this->remainder_bin = 0;
};

////////////////////////////////////////////////////////////////////////////////
// HISTOGRAM::SETINDIVIDUALWIDTH
// -----------------------------
// METHOD WHICH CAN BE USED TO SET A USER DEFINED BIN WIDTH
//
// IN:
//   bin_width_ (VECTOR<DOUBLE>) ... VECTOR CONTAINING WIDTH OF EACH BIN
// OUT:
//   (INT) 0 IF SUCCESSFULL, 1 OTHERWISE
////////////////////////////////////////////////////////////////////////////////
int Histogram::setIndividualWidth(std::vector<double> bin_width_) {
    /* CALCULATE TOTAL WIDTH OF BARS */
    double sum = 0;
    for (int j = 0; j < this->num_bins; j++)
        sum += bin_width_[j];
    /* THROW ERROR IF TOTAL WIDTH DOES NOT MATCH RANGE SET IN CONSTRUCTOR */
    if (sum > this->range * (1 + PU_TOLERANCE) or 
            sum < this->range * (1 - PU_TOLERANCE)) {
        std::cerr << "HISTOGRAM: BIN-WIDTH DOES NOT MATCH PREVIOUS SETTINGS!"
                << std::endl;
        return 1;
    } else {
        /* SET BIN WIDTH IF CHECK ABOVE RESULTS IN VALID WIDTHS */
        this->bin_width = bin_width_;
        return 0;
    }
};

////////////////////////////////////////////////////////////////////////////////
// HISTOGRAM::SETMAXVALS
// ---------------------
// METHOD SETTING THE MAXIMUM VALUES FOR EACH BIN DEPENDING ON NUMBER OF BINS,
// TOTAL MINIMUM VALUE AND TOTAL MAXIMUM VALUE
//
// IN:
//   -
// OUT:
//   -
////////////////////////////////////////////////////////////////////////////////
void Histogram::setMaxVals() {
    /* CREATE MAXIMUM VALUE VECTOR */
    this->maxvals = *(new std::vector<double> (this->num_bins));
    /* SET LOWEST MAX VALUE */
    this->maxvals[0] = this->min + this->bin_width[0];
    /* SET MAX VALUE OF EACH BIN ACCORDING TO THE PREVIOUS ONE */
    for (int j = 1; j < this->num_bins; j++)
        this->maxvals[j] = this->maxvals[j - 1] + this->bin_width[j];
};

////////////////////////////////////////////////////////////////////////////////
// HISTOGRAM::SETDATA
// ------------------
// SETS THE DATA TO BE PLOTTED WITH THE NEXT plot COMMAND
//
// IN:
//   data (VECTOR<DOUBLE>) ... DATA TO PROCESS FOR NEXT PLOT
// OUT:
//   -
////////////////////////////////////////////////////////////////////////////////
void Histogram::setData(std::vector<double> data) {
    /* SET MAXIMUM VALUES FOR EACH BIN */
    Histogram::setMaxVals();
    /* ITERATE OVER EACH DATA POINT GIVEN */
    for (auto d : data) {
        /* EACH DATA POINT INCREMENTS THE NUMBER OF TOTAL ELEMENTS BY 1 */
        this->total_elements++;
        /* IF IT DOES NOT FIT IN [min, max], THEN PUSH IT IN REMAINDER BIN */
        if (d < this->min || d > this->max) {
            this->remainder_bin++;
            continue;
        }
        /* OTHERWISE CHECK IN WHICH BIN IT BELONGS AND ADD IT THERE */
        for (int j = 0; j < this->num_bins; j++) {
            if (d < maxvals[j]) {
                this->bin[j]++;
                break;
            }
        }  
    }
};

////////////////////////////////////////////////////////////////////////////////
// HISTOGRAM::GETXTICKS
// --------------------
// CREATES STRINGS FOR A GIVEN NUMBER OF TICKS ON X AXIS
//
// IN:
//   num_entries (INT) ... NUMBER OF X TICKS
// OUT:
//   (VECTOR<STRING>) VECTOR CONTAINING ALL X TICKS AS STRINGS
////////////////////////////////////////////////////////////////////////////////
std::vector<std::string> Histogram::getXTicks(int num_entries) {  
    /* GET NUMBER OF BINS FOR EACH TICK (ENTRY) */
    int bins_per_entry = (this->num_bins - 1) / (num_entries - 1);
    /* DECLARE VECTOR OF STRINGS FOR XTICKS */
    std::vector<std::string> xticks;
    /* ITERATE OVER ALL BINS WHICH GET AN XTICK AND SET XTICK TO CONTAIN 
       THE X VALUE AT THIS BIN */
    for (int j = 1; j < num_bins; j += bins_per_entry) {
        xticks.push_back(str(boost::format("\"%.2f\" %d") 
                % this->maxvals[j - 1] % j));
    }
    /* RETURN ALL X TICKS */
    return xticks;
};

////////////////////////////////////////////////////////////////////////////////
// HISTOGRAM::PLOT
// ---------------
// COMMAND TO PLOT HISTOGRAM OF GIVEN VARIANT
//
// IN:
//   variant (INT) ... VARIANT OF UNCERTAINTY (SEE BELOW)
// OUT:
//   -
////////////////////////////////////////////////////////////////////////////////
void Histogram::plot(int variant) {
    this->plot(variant, "");
};


////////////////////////////////////////////////////////////////////////////////
// HISTOGRAM::PLOT
// ---------------
// COMMAND TO PLOT HISTOGRAM WITH GIVEN VARIANT OF UNCERTAINTY BARS AND
// AN ADDITIONAL ANALYTICAL FUNCTION GIVEN AS A STRING
//
// IN:
//   variant        (INT)    ... VARIANT OF UNCERTAINTY. POSSIBLE VALUES:
//                                 0: NO UNCERTAINTY BARS
//                                 1: USE FREQUENTIST APPROACH (NOT NORMALIZED)
//                                 2: USE FREQUENTIST APPROACH (NORMALIZED)
//                                 3: USE BAYESIAN APPROACH (RECOMMENDED!)
//   lineplotstring (STRING) ... STRING CONTAINING ADDITIONAL FUNCTION TO PLOT 
// OUT:
//   -
////////////////////////////////////////////////////////////////////////////////
void Histogram::plot(int variant, std::string lineplotstring) {
    /* DECLARE VECTOR CONTAINING UNCERTAINTIES FOR EACH BIN */
    std::vector<double> uncert;
    /* CREATE VECTOR CONTAINING BIN ENTRIES READY FOR PLOT (E.G. NORMALIZED) */
    std::vector<double> plotbins (this->num_bins);
    /* CALCULATE UNCERTAINTY DEPENDING ON VARIANT */
    switch (variant) {
        case 0: /* NO UNCERTAINTY */
            /* DIRECTLY PLOT DATA AND RETURN IMMEDIATELY */
            gp << "plot '-' with boxes title 'Histogram bars'\n";
            gp.send1d(this->bin);
            return;
        case 1: /* FREQUENTIST APPROACH (NOT NORMALIZED) */
            /* CALCULATE UNCERTAINTY */
            uncert = Histogram::frequentistUncertainty();
            /* COPY BIN VALUES TO PLOTBIN VECTOR AS DOUBLE */
            for (int j = 0; j < this->num_bins; j++)
                plotbins[j] = (double) this->bin[j];
            break;
        case 2: /* FREQUENTIST APPROACH (NORMALIZED) */
            /* CALCULATE UNCERTAINTY */
            uncert = Histogram::frequentistUncertainty();
            /* NORMALIZE EACH BIN */
            for (int j = 0; j < this->num_bins; j++) {
                plotbins[j] = (double) this->bin[j] / this->total_elements;
                uncert[j] /= this->total_elements;
            }
            break;
        case 3: /* BAYESIAN APPROACH (RECOMMENDED!) */
            /* NORMALIZE EACH BIN */
            for (int j = 0; j < this->num_bins; j++)
                plotbins[j] = ((double) this->bin[j] + 1) / 
                        (this->total_elements + this->num_bins + 1);
            /* CALCULATE UNCERTAINTY */
            uncert = Histogram::bayesianUncertainty(plotbins);
            break;
        default: /* IF VARIANT NOT KNOWN, THROW ERROR */
            std::cout << "Unrecognized plotting variant: " << variant << std::endl;
            return;
    }
    /* SET HISTOGRAM TO CONTAIN NO GAPS AND SET LINE WIDTH */
    this->gp << "set style histogram errorbars gap 0 lw 1\n";
    /* SET DATA STYLE TO BE A HISTOGRAM */
    this->gp << "set style data histogram\n";
    /* GET XTICKS (ARGUMENT IS ARBITRARY BUT 5 SEEMS A GOOD AMOUNT) */
    std::vector<std::string> xticks = Histogram::getXTicks(5);
    /* SET INTERNAL XRANGE TO BE NATURAL NUMBERS INCREMENTING FOR EACH BIN */
    this->gp << "set xrange [0:" << this->num_bins << "]\n";
    /* SET XTICKS ACCORDING TO VALUES GOTTEN FROM METHOD ABOVE */
    this->gp << "set xtics (";
    for (auto tic : xticks)
        gp << tic << ",";
    this->gp << ")\n";
    /* IF A LINEPLOTSTRING IS SUPPLIED, PLOT IT TOO */
    if (lineplotstring != "") {
        this->gp << "x = \"(x * " << this->range / this->num_bins << " + " << 
            this->min << ")\"\n";
        this->gp << "plot " << this->bin_width[0] << " * " << lineplotstring << 
            " with lines title 'Theoretical PDF'\n";
        this->gp << "re";
    }
    /* PLOT HISTOGRAM DATA. DATA PAIRS NEED TO BE PASSED AS TUPLES */
    this->gp << "plot '-' using 1:2 title 'Histogram bars'\n";
    this->gp.send1d(boost::make_tuple(plotbins, uncert));
};

////////////////////////////////////////////////////////////////////////////////
// HISTOGRAM::FREQUENTISTUNCERTAINTY
// ---------------------------------
// METHOD RETURNING THE UNCERTAINTY OF BIN VALUES USING THE FREQUENTIST APPROACH
//
// IN:
//   -
// OUT:
//   (VECTOR<DOUBLE>) UNCERTAINTY OF BIN VECTOR USING FREQUENTIST APPROACH
////////////////////////////////////////////////////////////////////////////////
std::vector<double> Histogram::frequentistUncertainty() {
    std::vector<double> uncert (this->num_bins);
    for (int b = 0; b < this->num_bins; b++) {
        uncert[b] = sqrt(this->bin[b] * (1 - this->bin[b] / this->total_elements));
    }
    return uncert;
};

////////////////////////////////////////////////////////////////////////////////
// HISTOGRAM::BAYESIANUNCERTAINTY
// ------------------------------
// METHOD RETURNING THE UNCERTAINTY OF BIN VALUES USING A BAYESIAN APPROACH
//
// IN:
//   avg_pi (VECTOR<DOUBLE>) ... VECTOR CONTAINING VALUE OF ENTRIES
// OUT:
//   (VECTOR<DOUBLE>) UNCERTAINTY OF BIN VECTOR USING BAYESIAN APPROACH
////////////////////////////////////////////////////////////////////////////////
std::vector<double> Histogram::bayesianUncertainty(std::vector<double> avg_pi){
    std::vector<double> uncert (this->num_bins);
    for (int b = 0; b < this->num_bins; b++)
        uncert[b] = sqrt(avg_pi[b] * (1 - avg_pi[b]) / 
                (this->total_elements + this->num_bins + 2));
    return uncert;
};

////////////////////////////////////////////////////////////////////////////////
// HISTOGRAM::PRINTBINS
// --------------------
// METHOD PRINTING THE NUMBER OF ENTRIES IN EACH BIN TO STDOUT
//
// IN:
//   -
// OUT:
//   -
////////////////////////////////////////////////////////////////////////////////
void Histogram::printBins() {
    std::cout << "Remainder bin elements: " << this->remainder_bin << std::endl;
    std::cout << this->min << " to " << this->maxvals[0] << ": " << this->bin[0]
            << std::endl;
    for (int j = 0; j < this->num_bins - 1; j++)
        std::cout << this->maxvals[j] << " to " << this->maxvals[j + 1] << ": "
                << this->bin[j + 1] << std::endl;
};

////////////////////////////////////////////////////////////////////////////
// HISTOGRAM::_SET
// ---------------
// METHOD WHICH SETS A GIVEN KEY TO A GIVEN VALUE
//
// IN:
//   key         (STRING) ... KEY TO CHANGE
//   value       (STRING) ... VALUE TO CHANGE KEY INTO
//   delim_left  (STRING) ... LEFT DELIMITER ADDED TO VALUE
//   delim_right (STRING) ... RIGHT DELIMITER ADDED TO VALUE
// OUT:
//   -
////////////////////////////////////////////////////////////////////////////
void Histogram::_set(std::string key, std::string value,
        std::string delim_left = "\"", std::string delim_right = "\"") {
    this->gp << "set " << key << " " << delim_left << value << delim_right << "\n";
    this->gp << "replot\n";
}

////////////////////////////////////////////////////////////////////////////
// HISTOGRAM::XLABEL
// -----------------
// METHOD WHICH SETS THE LABEL OF THE x AXIS
//
// IN:
//   label_str (STRING) ... STRING WHICH GETS SET AS THE LABEL OF THE x AXIS
// OUT:
//   -
////////////////////////////////////////////////////////////////////////////
void Histogram::xlabel(std::string label_str) {
    this->_set("xlabel", label_str);
}

////////////////////////////////////////////////////////////////////////////
// HISTOGRAM::YLABEL
// -----------------
// METHOD WHICH SETS THE LABEL OF THE y AXIS
//
// IN:
//   label_str (STRING) ... STRING WHICH GETS SET AS THE LABEL OF THE y AXIS
// OUT:
//   -
////////////////////////////////////////////////////////////////////////////
void Histogram::ylabel(std::string label_str) {
    this->_set("ylabel", label_str);
}

////////////////////////////////////////////////////////////////////////////
// HISTOGRAM::TITLE
// ----------------
// METHOD WHICH SETS THE TITLE OF THE WHOLE FIGURE
//
// IN:
//   title_str (STRING) ... STRING WHICH GETS SET AS THE FIGURE SUPERTITLE
// OUT:
//   -
////////////////////////////////////////////////////////////////////////////
void Histogram::title(std::string title_str) {
    this->_set("title", title_str);
}
// ########################## END HISTOGRAM CLASS ########################### //

} // end namespace

#endif /* PLOTUTILS_H */
