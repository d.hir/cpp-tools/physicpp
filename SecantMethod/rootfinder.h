////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////
////  REALQPDSE.H
////  ===========
////
////  HEADER FILE FOR SOLVING THE QUARK PROPAGATOR DYSON SCHWINGER EQUATION
////  ON THE REAL AXIS.
////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifndef ROOTFINDER_H
#define ROOTFINDER_H

/* TO PRINT RUNNING INFOS ON CURRENT PROGRESS AND PRINT WARNINGS / ERRORS */
#include <iostream>
/* TO SET PRECISION OF OUTPUT STREAM */
#include <iomanip>

/* MAXIMUM AMOUNT OF DEVIATION BETWEEN ITERATIONS TO BE CONSIDERED CONVERGED */
#ifndef ROOTFINDER_CONVERGENCE_EPSILON
#define ROOTFINDER_CONVERGENCE_EPSILON 1e-6
#endif /* ROOTFINDER_CONVERGENCE_EPSILON */

/* MAXIMUM NUMBER OF ITERATIONS BEFORE ABORTING SEARCH */
#ifndef ROOTFINDER_MAX_ITERATIONS
#define ROOTFINDER_MAX_ITERATIONS 100
#endif /* ROOTFINDER_MAX_ITERATIONS */

////////////////////////////////////////////////////////////////////////////////
// 
// ROOTFINDER CLASS
// ================
//
// CLASS USED FOR FINDING THE ROOT OF A GIVEN FUNCTION.
// 
////////////////////////////////////////////////////////////////////////////////
template<typename T>
class RootFinder {
private:
    /* PREVIOUSLY GIVEN x AND y VALUES */
    T x_prev;
    T y_prev;

    ////////////////////////////////////////////////////////////////////////////
    // ROOTFINDER::ROOTFINDER
    // ----------------------
    // CONSTRUCTOR FOR ROOTFINDER OBJECTS.
    // THIS METHOD IS PRIVATE. ONE SHOULD ONLY INTERACT WITH THIS MODULE USING
    // THE STATIC FINDROOT METHOD.
    //
    // IN:
    //   x0 (T) ... X VALUE OF FIRST DATA PAIR
    //   y0 (T) ... Y VALUE OF FIRST DATA PAIR
    // OUT:
    //   (ROOTFINDER)
    ////////////////////////////////////////////////////////////////////////////
    RootFinder(T x0, T y0) : x_prev(x0), y_prev(y0) {}

    ////////////////////////////////////////////////////////////////////////////
    // ROOTFINDER::SECANTMETHOD
    // ------------------------
    // METHOD APPLYING THE SECANT METHOD TO GET THE X VALUE FOR THE NEXT 
    // ITERATION.
    //
    // IN:
    //   x_new (T) ... X VALUE OF NEW DATA PAIR
    //   y_new (T) ... Y VALUE OF NEW DATA PAIR
    // OUT:
    //   (T) PROPOSAL X VALUE FOR NEXT ITERATION
    ////////////////////////////////////////////////////////////////////////////
    T secantMethod (T x_new, T y_new) {
        /* CALCULATE NEW X VALUE */
        T x_proposal = (x_prev * y_new - x_new * y_prev) / (y_new - y_prev);
        /* (x_new, y_new) IS FROM NOW ON CONSIDERED THE PREVIOUS DATA PAIR */
        x_prev = x_new;
        y_prev = y_new; 
        /* RETURN NEW X VALUE */
        return x_proposal;
    }

public:
    ////////////////////////////////////////////////////////////////////////////
    // ROOTFINDER::FINDROOT
    // --------------------
    // STATIC METHOD WHICH RETURNS THE ROOT OF A FUNCTION PASSED AS THE FIRST
    // ARGUMENT (DATATYPE TEMPLATED) IN THE INTERVAL [x_lower, x_higher].
    //
    // IN:
    //   fct      (FCT_T) ... FUNCTION POINTER OR FUNCTOR WHOSE ROOT IS WANTED
    //   x_lower  (T)     ... FIRST PROPOSED X VALUE
    //   x_higher (T)     ... SECOND PROPOSED X VALUE
    // OUT:
    //   (T) ROOT OF fct
    ////////////////////////////////////////////////////////////////////////////
    template<typename fct_t>
    static T findRoot(fct_t fct, T x_lower, T x_higher) {
        /* PRINT DEBUG INFOS IF ASKED TO */
#ifdef ROOTFINDER_PRINT_RUNTIME_INFOS
        /* MAKE SURE, OUTPUTTED NUMBERS ARE WRITTEN PRECISE ENOUGH */
        std::cout << std::setprecision(10);
        /* INITIALLY PRINT CALCULATION 1 TO STDOUT */
        std::cout << "[I:ROOTFINDER] Calculation 1:\n";
        /* AT THE BEGINNING, fct(x_lower) WILL BE EVALUATED */
        std::cout << "[I:ROOTFINDER] Calculating function at " 
                << x_lower << "\n";
#endif /* ROOTFINDER_PRINT_RUNTIME_INFOS */
        /* CALCULATE fct(x_lower) */
        T y_val = fct(x_lower);
        /* PRINT DEBUG INFOS IF ASKED TO */
#ifdef ROOTFINDER_PRINT_RUNTIME_INFOS
        /* PRINT CALCULATED FUNCTION VALUE */
        std::cout << "[I:ROOTFINDER] Found function value " 
                << y_val << "\n";
#endif /* ROOTFINDER_PRINT_RUNTIME_INFOS */

        /* CREATE ROOTFINDER OBJECT WITH FIRST DATA PAIR */
        RootFinder<T> rf (x_lower, y_val);

        /* SET NEW AND OLD X VALUES */
        T new_x = x_higher;
        T prev_x = x_lower;
        /* INITIALIZE THE VARIABLE COUNTING THE NUMBER OF ITERATIONS TO 0 */
        int iteration_counter = 0;
        /* DO AS LONG AS NOT CONVERGED AND MAXIMUM ITERATIONS NOT REACHED */
        while (std::fabs(new_x - prev_x) > ROOTFINDER_CONVERGENCE_EPSILON &&
                iteration_counter < ROOTFINDER_MAX_ITERATIONS) {
            /* SET NEW X VALUE TO BE THE PREVIOUS ONE */
            prev_x = new_x;
            /* PRINT DEBUG INFOS IF ASKED TO */
#ifdef ROOTFINDER_PRINT_RUNTIME_INFOS
            /* PRINT NUMBER OF ITERATION TO STDOUT */
            std::cout << "[I:ROOTFINDER] Calculation " 
                    << iteration_counter + 2 << ":\n";
            /* PRINT x VALUE WHERE FUNCTION WILL BE EVALUATED TO STDOUT */
            std::cout << "[I:ROOTFINDER] Calculating function at " 
                    << prev_x << "\n";
#endif /* ROOTFINDER_PRINT_RUNTIME_INFOS */
            /* GET FUNCTION VALUE AT prev_x */
            y_val = fct(prev_x);
            /* PRINT DEBUG INFOS IF ASKED TO */
#ifdef ROOTFINDER_PRINT_RUNTIME_INFOS
            /* PRINT FOUND FUNCTION VALUE TO STDOUT */
            std::cout << "[I:ROOTFINDER] Found function value " 
                    << y_val << "\n";
#endif /* ROOTFINDER_PRINT_RUNTIME_INFOS */
            /* USE SECANT METHOD TO GET NEW X VALUE */
            new_x = rf.secantMethod(prev_x, y_val);
            /* INCREMENT ITERATION COUNTING VARIABLE */
            iteration_counter++;
        }
        /* WARN IF LOOP FINISHED BECAUSE MAX ITERATIONS WAS REACHED */
        if (iteration_counter == ROOTFINDER_MAX_ITERATIONS)
            std::cout << "[W:ROOTFINDER] WARNING: MAX ITERATIONS REACHED\n";
        /* IF PRINTING RUNTIME INFOS, PRINT SUCCESSFULL RESULT ALSO TO STDOUT */
#ifdef ROOTFINDER_PRINT_RUNTIME_INFOS
        else
            std::cout << "[I:ROOTFINDER] Converged on " << new_x << "\n";
        /* PRINT THAT CALCULATION IS DONE */
        std::cout << "[I:ROOTFINDER] RootFinder done\n";
#endif
        /* RETURN ROOT OF fct */
        return new_x;
    }
};

#endif /* ROOTFINDER_H */